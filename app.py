import os
import sys
import base64
import json
import requests

try:
    from flask import Flask
    from flask import render_template
    from tinydb import TinyDB, Query
except Err:
    os.system('clear')
    print("[!] Error has occured: ", Err)
    sys.exit(1)

#############################################

def decrypt_token(file_name):
   data = open(file_name)
   file_data = data.readline()
   data.close()
   return str(base64.b64decode(file_data),'utf-8') 

def get_list(network, secret):
    zerotier_url = 'https://my.zerotier.com/api/v1/network/{}/member'.format(network)
    head = {'Authorization': 'bearer {}'.format(secret)}
    try:
        response =  requests.get(zerotier_url,headers=head)
    except TypeError:
        return 'Something went wrong'
    return response, response.content
#############################################
app = Flask(__name__)
secret = decrypt_token('.tmp.access.key')

@app.route('/')
def index():
    return f'<h1>Welcome to RestAPI applciation {app} </h1>'

@app.route('/members')
def members():
    status , data = get_list('6ab565387a4a08b6',secret)
    print(status)
    return tuple(json.loads(data))